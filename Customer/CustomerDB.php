<?php

namespace Customer;

class CustomerDB implements ICustomer
{
	use CustomerTrait;

	public function getCustomer(int $id)
	{
		echo "get customer $id from database";
	}

	public function getCustomerFullName(int $id)
	{
		echo "get customer $id full name from database";
	}
}
