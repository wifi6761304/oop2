<?php
namespace Customer;

interface ICustomer {
	public function getCustomer(int $id);
	public function getCustomerFullName(int $id);
}