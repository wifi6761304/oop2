<?php

namespace Customer;

class CustomerCSV implements ICustomer
{
	use CustomerTrait;
	public function getCustomer(int $id)
	{
		echo "get customer $id from csv file";
	}

	public function getCustomerFullName(int $id)
	{
		echo "get customer $id full name from csv file";
	}
}
