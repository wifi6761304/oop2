<?php
namespace System;

class Helper {
	public static function countCharacters(string $string): int
	{
		return strlen($string);
	}
}