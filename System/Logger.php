<?php

namespace System;

class Logger
{
	private static $file = 'log/log.csv';

	/**
	 * Logs current datetime (Y-m-d H:i), type and message to a file
	 * delimted by a semicolon in a new line
	 *
	 * @param string $message
	 * @param string $type
	 * @return void
	 */
	public static function log(string $message, string $type = 'info')
	{
		$date = date('Y-m-d H:i');
		$file = fopen(__DIR__ . '/../' . self::$file, 'a');
		if ($file !== false) {
			fputcsv(
				stream: $file,
				fields: [$date, $type, $message],
				separator: ';'
			);
			// fwrite($file, "$date;$type;$message\n");
			fclose($file);
		}
	}
}
