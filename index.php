<?php require_once __DIR__ . '/vendor/autoload.php'; ?><!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>OOP Advanced</title>
</head>
<body>
	<h1>OOP Advanced Topics</h1>
	<?php
		use System\Logger;
		$customer = new Customer\CustomerCSV();
		$customer->getCustomer(12);
		echo '<br>';
		$customer->getCustomerFullName(12);
		echo '<br>';
		$customer->testTrait();
		echo '<br>';

		$customer2 = new Customer\CustomerDB();
		echo $customer2->testTrait();
		echo '<br>';
		echo $customer2->showCustomer();
		echo '<br>';

		echo System\Helper::countCharacters('Jipie');
		Logger::log('Jipie, ein Eintrag', 'info');
		Logger::log('Oh nein, ein Fehler', 'error');
	?>
</body>
</html>